#include <iostream>
#include <string>
#include <vector>
#include <math.h>

using namespace std;

class Point{
private:
	double x, y;
public:
	Point(){}

	istream& operator>>(istream& cin)
	{
		cin >> x >> y;
		return cin;
	}

	bool operator==(Point  const &a){
		if (this->x == a.x && this->y == a.y)
			return true;
		else
			return false;
	}

	bool operator>(Point const &a){
		if (this->x > a.x && this->y > a.y)
			return true;
		else
			return false;
	}

	bool operator<(Point const &a){
		if (this->x < a.x && this->y < a.y)
			return true;
		else
			return false;
	}

	Point operator*(Point const & a){
		Point res;
		res.x = (this->x * a.x);
		res.y = (this->y * a.y);
		return res;
	}

	Point operator-(Point const & a){
		Point res;
		res.x = (this->x - a.x);
		res.y = (this->y - a.y);
		return res;
	}

	Point operator/(int const & a){
		Point res;
		res.x = (this->x / a);
		res.y = (this->y / a);
		return res;
	}

	Point(double x, double y) {
		this->x = x;
		this->y = y;
	}

	void print() {
		cout << "(" << x << ", " << y << ")" << endl;
	}

	double getX() {
		return x;
	}

	double getY() {
		return y;
	}
};

class Square{
private:
	Point a, b, c, d;
	double r, p;
	
public:
	Square(){}

	Square(Point a, Point b, Point c, Point d){
		//if (a.getX() - b.getX() == c.getX() - d.getX() && a.getY() - b.getY() == c.getY() - d.getY() && a.getX() - b.getX() == a.getY() - c.getY())
		this->r = a.getX() - b.getX();
		this->p = r*r;
		this->a = a;
		this->b = b;
		this->c = c;
		this->d = d;
	}

	bool operator==(Square const &b){
		if (this->a == b.a && this->b == b.b && this->c == b.c && this->d == b.d && this->p == b.p)
			return true;
		else
			return false;
	}
		
	~Square(){}

	double getP(){
		return p;
	}

	double getR(){
		return r;
	}

	Point getA(){
		return this->a;
	}

	Point getB(){
		return this->b;
	}

	Point getC(){
		return this->c;
	}

	Point getD(){
		return this->d;
	}

	void print(){
		cout << "Points: ";
		this->getA().print();
		this->getB().print();
		this->getC().print();
		this->getD().print();
		cout << endl;
	}

};

class alg{
private:
	Square a, b;

public:
	alg(Square a, Square b){
		this->a = a;
		this->b = b;
	}

	~alg(){}

	void area(Square a, Square b){
		if (a == b){
			cout << "Square are equals. Area is: " << a.getP() << endl << endl;
			return;
		}

		if (a.getA().getX() > b.getA().getX() && a.getA().getY() > b.getA().getY() &&
			a.getC().getX() > b.getC().getX() && a.getC().getY() < b.getC().getY() &&
			a.getB().getX() < b.getB().getX() && a.getB().getY() > b.getB().getY() &&
			a.getD().getX() < b.getD().getX() && a.getD().getY() < b.getD().getY()){
			
			cout << "First square less than second. Area is: " << a.getP() << endl << endl;
			return;
		}

		if (a.getA().getX() < b.getA().getX() && a.getA().getY() < b.getA().getY() &&
			a.getC().getX() < b.getC().getX() && a.getC().getY() > b.getC().getY() &&
			a.getB().getX() > b.getB().getX() && a.getB().getY() < b.getB().getY() &&
			a.getD().getX() > b.getD().getX() && a.getD().getY() > b.getD().getY()){

			cout << "Second square less than first. Area is: " << b.getP() << endl << endl;
			return;
		}

		if (a.getA().getX() < b.getA().getX() && a.getD().getX() > b.getA().getX() &&
			a.getA().getY() < b.getA().getY() && a.getD().getY() > b.getA().getY()){

			cout << "Area is: " << ((a.getD().getX() - b.getA().getX())*(a.getD().getY() - b.getA().getY())) << endl << endl;
			return;
		}
		

		if (a.getA().getX() < b.getD().getX() && a.getD().getX() > b.getD().getX() &&
			a.getA().getY() < b.getD().getY() && a.getD().getY() > b.getD().getY()){

			cout << "Area is: " << ((b.getD().getX() - a.getA().getX())*(b.getD().getY() - a.getA().getY())) << endl << endl;
			return;
		}

		if (a.getC().getX() < b.getB().getX() && a.getC().getX() > b.getC().getX() &&
			a.getC().getY() > b.getA().getY() && a.getC().getY() < b.getC().getY()){

			cout << "Area is: " << ((a.getC().getX() - b.getB().getX())*(b.getB().getY() - a.getC().getY())) << endl << endl;
			return;
		}

		if (b.getC().getX() < a.getB().getX() && b.getC().getX() > a.getC().getX() &&
			b.getC().getY() > a.getA().getY() && b.getC().getY() < a.getC().getY()){

			cout << "Area is: " << ((b.getC().getX() - a.getB().getX())*(a.getB().getY() - b.getC().getY())) << endl << endl;
			return;
		}

		cout << "Squares isn't intersected";
	}
};

int main(){
	Square *a = new Square(Point(0.0, 0.0), Point(4.0, 0.0), Point(0.0, 4.0), Point(4.0, 4.0));
	Square *b = new Square(Point(1.0, 1.0), Point(2.0, 1.0), Point(1.0, 2.0), Point(2.0, 2.0));
	a->print();
	b->print();
	alg *alg1 = new alg(*a, *b);
	alg1->area(*a, *b);

	Square *c = new Square(Point(0.0, 0.0), Point(4.0, 0.0), Point(0.0, 4.0), Point(4.0, 4.0));
	Square *d = new Square(Point(2.0, 2.0), Point(6.0, 2.0), Point(2.0, 6.0), Point(6.0, 6.0));
	c->print();
	d->print();

	alg *alg2 = new alg(*c, *d);
	alg1->area(*c, *d);

	Square *e = new Square(Point(0.0, 0.0), Point(4.0, 0.0), Point(0.0, 4.0), Point(4.0, 4.0));
	Square *f = new Square(Point(2.0, 2.0), Point(6.0, 2.0), Point(2.0, 6.0), Point(6.0, 6.0));
	f->print();
	e->print();
	alg *alg3 = new alg(*f, *e);
	alg1->area(*f, *e);

	Square *g = new Square(Point(2.0, 0.0), Point(6.0, 0.0), Point(2.0, 4.0), Point(6.0, 4.0));
	Square *h = new Square(Point(0.0, 2.0), Point(4.0, 2.0), Point(0.0, 6.0), Point(4.0, 6.0));
	g->print();
	h->print();
	alg *alg4 = new alg(*g, *h);
	alg4->area(*h, *g);
}